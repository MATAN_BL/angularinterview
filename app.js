/**
 * Created by nataliebarnatan on 06/07/2017.
 */
var express = require ('express');
var app = express();

app.use("/", express.static("./public"));

app.get("/getPersonsData", function(req, res) {
    res.sendFile("public/js/mock.json", {root:__dirname});
})

app.listen(process.env.PORT || 3000);
console.log('listening on port 3000');
change in server
