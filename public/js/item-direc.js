app.directive('item', function() {
    return {
        templateUrl: "./item.html",
        link: function (scope, elem, attrs) {
            var details = JSON.parse(attrs.myattr);
            scope.person = {};
            scope.person.firstName = details.firstName;
            scope.person.lastName = details.lastName;
            scope.person.title = details.title;
            scope.person.image = details.profileImageURL;

            scope.company = {};
            scope.company.companyName = details.companyName;
            scope.company.companyLogo = details.companyLogo;
            var industries = details.topLevelIndustry;
            scope.company.topLevelIndustry = typeof(industries) === "string" ? industries : industries.join(', ');
            scope.company.companyRevenue = details.companyRevenue;
            scope.company.companyEmployeeCountRange = details.companyEmployeeCountRange;
            scope.company.companyState = details.companyAddress['State'];
            scope.company.companyCity = details.companyAddress['City'];
            scope.company.lastUpdatedDate = details.lastUpdatedDate;
        }
    }
})